const express = require('express');
const nanoid = require('nanoid/generate');
const router = express.Router();

const Link = require('../models/Link');

const createRouter = () => {

  router.post('/', (req, res) => {
    const linkData = req.body;

    linkData.shortUrl =
      nanoid('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 7);

    const link = new Link(linkData);

    link.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:shortUrl', (req, res) => {
    const shortUrl = req.params.shortUrl;

    Link.findOne({shortUrl}, (err, result) => {
      if (err) res.status(404).send(err);
      res.status(301).redirect(result.baseUrl);
    })
  });

  return router;
};

module.exports = createRouter;