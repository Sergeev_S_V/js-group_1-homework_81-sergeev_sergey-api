const path = require('path');

const rootPath = __dirname; // какой путь здесь хранится

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'links'
  }
};

