const mongoose = require('mongoose');

const LinksSchema = new mongoose.Schema({
  shortUrl: {
    type: String, unique: true
  },
  baseUrl: {
    type: String, required: true
  }
});

const Link = mongoose.model('Link', LinksSchema);

module.exports = Link;